#pragma once

#include "arena.h"
#include "mystate.h"

#define Dst       ms
#define Dst_DECL  MyState *ms
#define Dst_REF   (ms->ds)

#define DASM_CHECKS
#include "dasm_proto.h"

typedef int (int_plus_const_func)(int);

void jit_init(Dst_DECL, Arena *arena);
void jit_free(Dst_DECL);
int_plus_const_func *jit_addition_with_constant(Dst_DECL, int constant);
